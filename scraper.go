package main

import (
	"encoding/csv"
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/gocolly/colly"
)

func scrap(search string) {
	fmt.Println("Trying to create the data file...")
	var fileName string = search + ".csv"
	var file, err = os.Create(fileName)

	if err != nil {
		log.Fatal("Cannot create file %q: %s \n", fileName, err)
	}

	fmt.Println("File %q successfully created!", fileName)

	defer file.Close()
	var writer = csv.NewWriter(file)
	defer writer.Flush()

	writer.Write([]string{"Name", "Price (MX)"})

	var collector = colly.NewCollector()

	collector.OnHTML(".ui-search-result__content-wrapper", func(e *colly.HTMLElement) {
		fmt.Println("Writing the data into the file...")
		writer.Write([]string{
			e.ChildText("h2.ui-search-item__title"),
			e.ChildText("span.price-tag-fraction"),
		})
	})

	fmt.Println("Accessing MercadoLibre and getting the data...")
	collector.Visit("https://listado.mercadolibre.com.mx/" + search)

	log.Printf("Scraping finished, check file %q for results\n", fileName)
}

const logo string = `            ________           _________                                        
	 /  _____/  ____    /   _____/ ________________  ______   ___________ 
	/   \  ___ /  _ \   \_____  \_/ ___\_  __ \__  \ \____ \_/ __ \_  __ \
	\    \_\  (  <_> )  /        \  \___|  | \// __ \|  |_> >  ___/|  | \/
	 \______  /\____/  /_______  /\___  >__|  (____  /   __/ \___  >__|   
	   		\/                 \/     \/           \/|__|        \/       
	_____                                   .___     .____    ._____.                  
   /     \   ___________   ____ _____     __| _/____ |    |   |__\_ |_________   ____  
  /  \ /  \_/ __ \_  __ \_/ ___\\__  \   / __ |/  _ \|    |   |  || __ \_  __ \_/ __ \ 
 /    Y    \  ___/|  | \/\  \___ / __ \_/ /_/ (  <_> )    |___|  || \_\ \  | \/\  ___/ 
 \____|__  /\___  >__|    \___  >____  /\____ |\____/|_______ \__||___  /__|    \___  >
		 \/     \/            \/     \/      \/              \/       \/            \/ `

func main() {
	fmt.Println(logo)

	var Argument []string = os.Args[1:]
	var search string = strings.Join(Argument, " ")

	fmt.Printf("Do you want to search %q? if not its time to press Ctrl + C haha!", search)

	scrap(search)
}
