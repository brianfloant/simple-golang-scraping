
# Simple Golang Scraping

A simple scraping cli made with go and colly.




## Installation

#### Cloning
If you have Go installed just clone and run this:

```bash
  go build scraper.go
```

#### Executable
Just go to the tag section and download the right zip for your system.
    
## Usage/Examples
It's quite simple, just run the file and pass whatever you want to search for as an argument, for example:

```bash
./scraper Nintendo
```
Then simply there will appear a [word-you-searched].csv file with everything found on the first page, ordered by name and price.

## Author

- [Brian Flores](https://gitlab.com/brianfloant/octokatherine)



